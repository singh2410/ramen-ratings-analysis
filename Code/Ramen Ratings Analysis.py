#!/usr/bin/env python
# coding: utf-8

# # Ramen_Ratings barplots analysis
# #By- Aarush Kumar
# #Dated: Sept. 20,2021

# In[1]:


from IPython.display import Image
Image(url='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDnmjwui8HDUFo5u31MuriyYE4h557hilFO3Gq3MswsVhh--iBWKuQEuY_yaoq6GYgJSc&usqp=CAU')


# In[2]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# In[3]:


data = pd.read_csv("/home/aarush100616/Downloads/Projects/Ramen Ratings Analysis/Data/ramen-ratings.csv")


# In[4]:


data


# In[5]:


data.shape


# In[6]:


data.size


# In[7]:


data.info()


# In[8]:


data.isnull().sum()


# In[9]:


data.describe()


# In[10]:


data.describe().T


# In[11]:


data['Country'].unique()


# In[12]:


data['Style'].unique()


# ## Data Cleaning

# In[13]:


data.Stars.value_counts()


# In[14]:


data.drop(data.loc[data['Stars']=='Unrated'].index, inplace=True)


# In[15]:


numeric_Stars = pd.to_numeric(data.Stars)
data.Stars = numeric_Stars


# In[16]:


japan_cup_data = data[(data.Style == 'Cup') & (data.Country == 'Japan')]
japan_cup_data


# In[17]:


japan_cup_brand_average_rating = japan_cup_data.groupby('Brand').Stars.mean()
japan_cup_brand_average_rating 


# In[18]:


sorted_brand_names = sorted(pd.Series(japan_cup_data.Brand.unique()))
sorted_brand_names


# ## Data Visualization

# In[19]:


sns.set_style("whitegrid") # "darkgrid", "whitegrid", "dark", "white", "ticks"
plt.figure(figsize = (8,6))
plt.title("Japan Cup Brand Average Rating", fontsize = 12)
sns.barplot(x = sorted_brand_names, y = japan_cup_brand_average_rating)
plt.xlabel("Brand")
plt.ylabel("Star Rating")
plt.xticks(rotation = 45)
plt.legend()
plt.show()


# In[20]:


#Shows the variety of cup ramen by each brand. 
brand_count = japan_cup_data.groupby('Brand').Brand.count()
brand_count


# In[21]:


pie, ax = plt.subplots(figsize=[10,6])
labels = sorted_brand_names
plt.pie(x= brand_count, autopct="%.1f%%", explode = [0.05]*9, labels=labels, pctdistance=0.8)
plt.title("Brand Count", fontsize=14);


# In[22]:


list_of_countries = data['Country'].unique()
list_of_styles = data['Style'].unique()  


# In[23]:


# Cleans the data by: dropping 'unrated' values and 
# changing object values to numerical ones in the Stars column.
def data_wrangling():
    data.drop(data.loc[data['Stars']=='Unrated'].index, inplace=True)
    numeric_Stars = pd.to_numeric(data.Stars)
    data.Stars = numeric_Stars


# In[24]:


def graph(country_name, style_name, x_values, y_values ):
    sns.set_style("whitegrid") # "darkgrid", "whitegrid", "dark", "white", "ticks"
    plt.figure(figsize = (8,6))
    plt.title("{}'s Average Star Rating of {} Ramen".format(country_name,style_name), fontsize = 12)
    sns.barplot(x = x_values, y = y_values)
    plt.xlabel("Brand")
    plt.ylabel("Star Rating")
    plt.xticks(rotation = 60)
    plt.legend()
    plt.show()


# In[25]:


# Extracts data and loops through to produce 2 Series. One of Brands 
# and another of average ratings to plot a barplot.
def extract_info():

    data_wrangling()
    for country in list_of_countries:
        for style in list_of_styles:
            entry = data[(data.Country == country) & (data.Style == style)]       #dataframe
            
            brand_average_rating = entry.groupby('Brand').Stars.mean()            #Series
            sorted_brand_names = sorted(pd.Series(entry.Brand.unique()))          #Series
            
            if len(sorted_brand_names) > 0:                                       
                graph(country,style,sorted_brand_names,brand_average_rating)      
            else:                                                                 
                pass
    
extract_info()

